namespace ConditionStatements3
{
    public static class Tasks
    {
        public static (int a, int b, int c) Task1(int a, int b, int c)
        {
            if ((a > b) && (b > c))
                return (c, b, a);
            else
               if ((a < b) && (b < c))
                return (a, b, c);
            else
               if ((a < b) && (a < c) && (b > c))
                return (a, b, b);
            else
               if ((c < a) && (c < b) && (b > c))
                return (c, b, b);
            else
               if ((b < a) && (b < c) && (a > c))
                return (b, b, a);
            else
                return (b, b, c);


        }


        public static (int a, int b, int c) Task2(int a, int b, int c)
        {
            if (a < b && b < c)
                return (a * 2, b * 2, c * 2);
            if (a < 0 && b < 0 && c < 0)
                return (Math.Abs(a), Math.Abs(b), Math.Abs(c));
            else
                return (a * -1, b * -1, c * -1);

        }

        public static (int a, int b, int c) Task3(int a, int b, int c)
        {
            if (a < b && b < c)
                return (a * 2, b * 2, c * 2);
            if (a > b && b > c)
                return (a * 2, b * 2, c * 2);
            return (a * -1, b * -1, c * -1);

        }
    }
}